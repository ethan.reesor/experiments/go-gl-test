package main

import (
	"log"
	"os"
	"runtime"

	"gitlab.com/ethan.reesor/go-gl-tests/engine"
	"gitlab.com/ethan.reesor/go-gl-tests/graphics"
	"gitlab.com/ethan.reesor/go-gl-tests/messaging"
)

func init() {
	// ensure that graphics.Main() runs on the OS main thread
	//   some functions must only be run on the main thread
	runtime.LockOSThread()
}

func nope(err error) {
	log.Printf("%+v", err)
	os.Exit(1)
}

func main() {
	bus := new(messaging.Bus)

	go run(bus)
	go engine.Main(bus)

	err := graphics.Main(bus)
	if err != nil {
		nope(err)
	}

	bus.Halt()
}

func run(bus *messaging.Bus) {
	alertG := messaging.Must(bus.Consumer(graphics.InvalidAlert)).(<-chan graphics.Alert)
	alertE := messaging.Must(bus.Consumer(engine.InvalidAlert)).(<-chan engine.Alert)

	defer bus.Halt()

	for {
		select {
		case x, ok := <-alertG:
			if !ok {
				return
			}

			switch x {
			case graphics.EscapeKey:
				return
			}

		case x, ok := <-alertE:
			if !ok {
				return
			}

			log.Print(x) // nothing to do yet
		}
	}
}
