package engine

import (
	"gitlab.com/ethan.reesor/go-gl-tests/messaging"
)

type context struct {
	bus   *messaging.Bus
	act0  <-chan action0
	alert chan<- Alert
}

func Main(bus *messaging.Bus) {
	c := &context{
		bus:   bus,
		act0:  messaging.Must(bus.Consumer(Close)).(<-chan action0),
		alert: messaging.Must(bus.Producer(InvalidAlert)).(chan<- Alert),
	}

	defer func() {
		close(c.alert)
	}()

	c.run()
}

func (c *context) run() {
	for {
		select {
		case x := <-c.act0:
			switch x {
			case Close:
				return
			}
		}
	}
}
