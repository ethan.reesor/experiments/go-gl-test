package messaging

import (
	"reflect"
	"sync"
	"sync/atomic"

	"golang.org/x/xerrors"
)

type Bus struct {
	lock      sync.Mutex
	halted    int32
	producers sync.WaitGroup
	consumers []*consumer
}

type consumer struct {
	ch   reflect.Value
	done interface{}
	send func(reflect.Value)
}

func IsValidBusType(typ reflect.Type) bool {
	for typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}
	switch typ.Kind() {
	case reflect.Bool, reflect.String,
		reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Float32, reflect.Float64, reflect.Complex64, reflect.Complex128:
		return true

	case reflect.Struct:
		for i := 0; i < typ.NumField(); i++ {
			if !IsValidBusType(typ.Field(i).Type) {
				return false
			}
		}
		return true

	default:
		return false
	}
}

func Must(v interface{}, err error) interface{} {
	if err != nil {
		panic(err)
	}
	return v
}

func (b *Bus) Dispatch(message interface{}) {
	b.dispatch(reflect.ValueOf(message))
}

func (b *Bus) dispatch(message reflect.Value) {
	for _, c := range b.consumers {
		c.send(message)
	}
}

func (b *Bus) Halt() {
	// stop all consumers (but only once)
	stop := atomic.CompareAndSwapInt32(&b.halted, 0, 1)

	if stop {
		for _, c := range b.consumers {
			c.ch.Send(reflect.ValueOf(c.done))
		}
	}

	// wait for producers to stop
	b.producers.Wait()

	// close consumers
	if stop {
		for _, c := range b.consumers {
			c.ch.Close()
		}
	}
}

func (b *Bus) Producer(v interface{}) (interface{}, error) {
	if v == nil {
		return nil, xerrors.Errorf("v must not be nil")
	}

	typ := reflect.TypeOf(v)
	if !IsValidBusType(typ) {
		return nil, xerrors.Errorf("%v (kind %v) is not a valid bus type", typ, typ.Kind())
	}

	ch := reflect.MakeChan(reflect.ChanOf(reflect.BothDir, typ), 10)

	b.producers.Add(1)
	go func() {
		defer b.producers.Done()

		for {
			v, ok := ch.Recv()
			if !ok {
				return
			}
			b.dispatch(v)
		}
	}()

	return ch.Convert(reflect.ChanOf(reflect.SendDir, typ)).Interface(), nil
}

func (b *Bus) Consumer(done interface{}) (interface{}, error) {
	if done == nil {
		return nil, xerrors.Errorf("done must not be nil")
	}

	typ := reflect.TypeOf(done)
	if !IsValidBusType(typ) {
		return nil, xerrors.Errorf("%v (kind %v) is not a valid bus type", typ, typ.Kind())
	}

	b.lock.Lock()
	defer b.lock.Unlock()

	ch := reflect.MakeChan(reflect.ChanOf(reflect.BothDir, typ), 10)
	c := &consumer{ch, done, func(v reflect.Value) {
		if v.Type().AssignableTo(typ) {
			ch.Send(v)
		}
	}}
	b.consumers = append(b.consumers, c)

	return ch.Convert(reflect.ChanOf(reflect.RecvDir, typ)).Interface(), nil
}
