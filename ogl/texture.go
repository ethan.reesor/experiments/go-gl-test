package ogl

import (
	"image"
	"image/draw"
	"os"

	"github.com/go-gl/gl/v4.6-core/gl"
)

func (tgt TextureTarget) Bind(tex Texture) { gl.BindTexture(tgt.ID(), tex.ID()) }
func (tgt TextureTarget) Unbind()          { gl.BindTexture(tgt.ID(), 0) }

func (tgt TextureTarget) NewFromImage(img image.Image) (Texture, error) {
	tex := tgt.New()

	tgt.Bind(tex)
	err := tgt.SetImage2D(img)
	tgt.Unbind()

	if err != nil {
		tex.Delete()
		return Texture{0}, nil
	}
	return tex, nil
}

func (tgt TextureTarget) NewFromImageFile(file string) (Texture, error) {
	tex := tgt.New()

	tgt.Bind(tex)
	err := tgt.LoadImageFile2D(file)
	tgt.Unbind()

	if err != nil {
		tex.Delete()
		return Texture{0}, nil
	}
	return tex, nil
}

func NewCubemapFromImageFiles(px, nx, py, ny, pz, nz string) (Texture, error) {
	tex := TextureCubeMap.New()
	TextureCubeMap.Bind(tex)
	defer TextureCubeMap.Unbind()

	err := TextureCubeMapPX.LoadImageFile2D(px)
	if err != nil {
		goto abort
	}

	err = TextureCubeMapNX.LoadImageFile2D(nx)
	if err != nil {
		goto abort
	}

	err = TextureCubeMapPY.LoadImageFile2D(py)
	if err != nil {
		goto abort
	}

	err = TextureCubeMapNY.LoadImageFile2D(ny)
	if err != nil {
		goto abort
	}

	err = TextureCubeMapPZ.LoadImageFile2D(pz)
	if err != nil {
		goto abort
	}

	err = TextureCubeMapNZ.LoadImageFile2D(nz)
	if err != nil {
		goto abort
	}

	return tex, nil

abort:
	tex.Delete()
	return Texture{0}, err
}

func (tgt TextureTarget) SetImage2D(img image.Image) error {
	rgba := image.NewRGBA(img.Bounds())
	draw.Draw(rgba, rgba.Bounds(), img, image.Pt(0, 0), draw.Src)
	size := rgba.Rect.Size()

	gl.TexImage2D(tgt.ID(), 0, gl.RGBA, int32(size.X), int32(size.Y), 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(rgba.Pix))
	return nil
}

func (tgt TextureTarget) LoadImageFile2D(file string) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	img, _, err := image.Decode(f)
	if err != nil {
		return err
	}

	return tgt.SetImage2D(img)
}
