package ogl

import (
	"github.com/go-gl/gl/v4.6-core/gl"
)

func (n Name) ID() uint32 { return uint32(n) }

func (n Name) getiv(name uint32, fn func(id, name uint32, value *int32)) (value int32) {
	fn(n.ID(), name, &value)
	return
}

func (n Name) geti64v(name uint32, fn func(id, name uint32, value *int64)) (value int64) {
	fn(n.ID(), name, &value)
	return
}

func (n Name) getuiv(name uint32, fn func(id, name uint32, value *uint32)) (value uint32) {
	fn(n.ID(), name, &value)
	return
}

func (n Name) getfv(name uint32, fn func(id, name uint32, value *float32)) (value float32) {
	fn(n.ID(), name, &value)
	return
}

func (n Program) Get(name uint32) int32           { return n.getiv(name, gl.GetProgramiv) }
func (n ProgramPipeline) Get(name uint32) int32   { return n.getiv(name, gl.GetProgramPipelineiv) }
func (n Query) Get(name uint32) int32             { return n.getiv(name, gl.GetQueryiv) }
func (n Shader) Get(name uint32) int32            { return n.getiv(name, gl.GetShaderiv) }
func (n TransformFeedback) Get(name uint32) int32 { return n.getiv(name, gl.GetTransformFeedbackiv) }
func (n VertexArray) Get(name uint32) int32       { return n.getiv(name, gl.GetVertexArrayiv) }

func (n BufferTarget) Getiv(name uint32) int32   { return n.getiv(name, gl.GetBufferParameteriv) }
func (n BufferTarget) Geti64v(name uint32) int64 { return n.geti64v(name, gl.GetBufferParameteri64v) }
func (n Sampler) Getiv(name uint32) int32        { return n.getiv(name, gl.GetSamplerParameteriv) }
func (n Sampler) GetIiv(name uint32) int32       { return n.getiv(name, gl.GetSamplerParameterIiv) }
func (n Sampler) GetIuiv(name uint32) uint32     { return n.getuiv(name, gl.GetSamplerParameterIuiv) }
func (n Sampler) Getfv(name uint32) float32      { return n.getfv(name, gl.GetSamplerParameterfv) }
