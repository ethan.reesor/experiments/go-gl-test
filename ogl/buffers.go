package ogl

import (
	"unsafe"

	"github.com/go-gl/gl/v4.6-core/gl"
)

const sizeof_f32 = int(unsafe.Sizeof(float32(0)))

func (tgt BufferTarget) Bind(b Buffer) { gl.BindBuffer(tgt.ID(), b.ID()) }
func (tgt BufferTarget) Unbind()       { gl.BindBuffer(tgt.ID(), 0) }

func (tgt BufferTarget) SetData(data []float32, usage Usage) {
	gl.BufferData(tgt.ID(), sizeof_f32*len(data), gl.Ptr(data), usage.ID())
}
