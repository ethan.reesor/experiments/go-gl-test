package ogl

import (
	"github.com/go-gl/gl/v4.6-core/gl"
)

var (
	ArrayBuffer             = BufferTarget{gl.ARRAY_BUFFER}
	AtomicCounterBuffer     = BufferTarget{gl.ATOMIC_COUNTER_BUFFER}
	CopyReadBuffer          = BufferTarget{gl.COPY_READ_BUFFER}
	CopyWriteBuffer         = BufferTarget{gl.COPY_WRITE_BUFFER}
	DispatchIndirectBuffer  = BufferTarget{gl.DISPATCH_INDIRECT_BUFFER}
	DrawIndirectBuffer      = BufferTarget{gl.DRAW_INDIRECT_BUFFER}
	ElementArrayBuffer      = BufferTarget{gl.ELEMENT_ARRAY_BUFFER}
	PixelPackBuffer         = BufferTarget{gl.PIXEL_PACK_BUFFER}
	PixelUnpackBuffer       = BufferTarget{gl.PIXEL_UNPACK_BUFFER}
	QueryBuffer             = BufferTarget{gl.QUERY_BUFFER}
	ShaderStorageBuffer     = BufferTarget{gl.SHADER_STORAGE_BUFFER}
	TextureBuffer           = BufferTarget{gl.TEXTURE_BUFFER}
	TransformFeedbackBuffer = BufferTarget{gl.TRANSFORM_FEEDBACK_BUFFER}
	UniformBuffer           = BufferTarget{gl.UNIFORM_BUFFER}
)

var (
	Points                 = Primitive{gl.POINTS}
	LineStrip              = Primitive{gl.LINE_STRIP}
	LineLoop               = Primitive{gl.LINE_LOOP}
	Lines                  = Primitive{gl.LINES}
	LineStripAdjacency     = Primitive{gl.LINE_STRIP_ADJACENCY}
	LinesAdjacency         = Primitive{gl.LINES_ADJACENCY}
	TriangleStrip          = Primitive{gl.TRIANGLE_STRIP}
	TriangleFan            = Primitive{gl.TRIANGLE_FAN}
	Triangles              = Primitive{gl.TRIANGLES}
	TriangleStripAdjacency = Primitive{gl.TRIANGLE_STRIP_ADJACENCY}
	TrianglesAdjacency     = Primitive{gl.TRIANGLES_ADJACENCY}
	Patches                = Primitive{gl.PATCHES}
)

var (
	SamplesPassed                      = QueryTarget{gl.SAMPLES_PASSED}
	AnySamplesPassed                   = QueryTarget{gl.ANY_SAMPLES_PASSED}
	AnySamplesPassedConservative       = QueryTarget{gl.ANY_SAMPLES_PASSED_CONSERVATIVE}
	TimeElapsed                        = QueryTarget{gl.TIME_ELAPSED}
	Timestamp                          = QueryTarget{gl.TIMESTAMP}
	PrimitivesGenerated                = QueryTarget{gl.PRIMITIVES_GENERATED}
	TransformFeedbackPrimitivesWritten = QueryTarget{gl.TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN}
)

var (
	ComputeShader                = ShaderType{gl.COMPUTE_SHADER}
	VertexShader                 = ShaderType{gl.VERTEX_SHADER}
	TessellationControlShader    = ShaderType{gl.TESS_CONTROL_SHADER}
	TessellationEvaluationShader = ShaderType{gl.TESS_EVALUATION_SHADER}
	GeometryShader               = ShaderType{gl.GEOMETRY_SHADER}
	FragmentShader               = ShaderType{gl.FRAGMENT_SHADER}
)

var (
	ProxyTexture1DArray       = TextureTarget{gl.PROXY_TEXTURE_1D_ARRAY}
	ProxyTexture2D            = TextureTarget{gl.PROXY_TEXTURE_2D}
	ProxyTextureCubeMap       = TextureTarget{gl.PROXY_TEXTURE_CUBE_MAP}
	ProxyTextureRectangle     = TextureTarget{gl.PROXY_TEXTURE_RECTANGLE}
	Texture1D                 = TextureTarget{gl.TEXTURE_1D}
	Texture1DArray            = TextureTarget{gl.TEXTURE_1D_ARRAY}
	Texture2D                 = TextureTarget{gl.TEXTURE_2D}
	Texture2DArray            = TextureTarget{gl.TEXTURE_2D_ARRAY}
	Texture2DMultisample      = TextureTarget{gl.TEXTURE_2D_MULTISAMPLE}
	Texture2DMultisampleArray = TextureTarget{gl.TEXTURE_2D_MULTISAMPLE_ARRAY}
	Texture3D                 = TextureTarget{gl.TEXTURE_3D}
	TextureCubeMap            = TextureTarget{gl.TEXTURE_CUBE_MAP}
	TextureCubeMapArray       = TextureTarget{gl.TEXTURE_CUBE_MAP_ARRAY}
	TextureCubeMapNX          = TextureTarget{gl.TEXTURE_CUBE_MAP_NEGATIVE_X}
	TextureCubeMapNY          = TextureTarget{gl.TEXTURE_CUBE_MAP_NEGATIVE_Y}
	TextureCubeMapNZ          = TextureTarget{gl.TEXTURE_CUBE_MAP_NEGATIVE_Z}
	TextureCubeMapPX          = TextureTarget{gl.TEXTURE_CUBE_MAP_POSITIVE_X}
	TextureCubeMapPY          = TextureTarget{gl.TEXTURE_CUBE_MAP_POSITIVE_Y}
	TextureCubeMapPZ          = TextureTarget{gl.TEXTURE_CUBE_MAP_POSITIVE_Z}
	TextureRectangle          = TextureTarget{gl.TEXTURE_RECTANGLE}
)

var (
	StreamDraw  = Usage{gl.STREAM_DRAW}
	StreamRead  = Usage{gl.STREAM_READ}
	StreamCopy  = Usage{gl.STREAM_COPY}
	StaticDraw  = Usage{gl.STATIC_DRAW}
	StaticRead  = Usage{gl.STATIC_READ}
	StaticCopy  = Usage{gl.STATIC_COPY}
	DynamicDraw = Usage{gl.DYNAMIC_DRAW}
	DynamicRead = Usage{gl.DYNAMIC_READ}
	DynamicCopy = Usage{gl.DYNAMIC_COPY}
)
