package ogl

import (
	"github.com/go-gl/gl/v4.6-core/gl"
)

func (p Primitive) Draw(first, count int32) { gl.DrawArrays(p.ID(), first, count) }
