package ogl

import (
	"github.com/go-gl/gl/v4.6-core/gl"
	"golang.org/x/xerrors"
)

func (typ ShaderType) Compile(source ...string) (Shader, error) {
	s := typ.New()
	s.Source(source...)
	err := s.Compile()
	if err != nil {
		s.Delete()
		return Shader{0}, err
	}

	return s, nil
}

func (s Shader) GetLog() string {
	n := s.Get(gl.INFO_LOG_LENGTH)
	if n == 0 {
		return ""
	}

	log := string(make([]byte, n+1))
	gl.GetShaderInfoLog(s.ID(), n, nil, gl.Str(log))
	return log
}

func (s Shader) Source(source ...string) {
	for i := range source {
		source[i] += "\x00"
	}

	sv, free := gl.Strs(source...)
	gl.ShaderSource(s.ID(), int32(len(source)), sv, nil)
	free()
}

func (s Shader) Compile() error {
	gl.CompileShader(s.ID())

	if s.Get(gl.COMPILE_STATUS) == gl.TRUE {
		return nil
	}

	log := s.GetLog()
	if log == "" {
		return xerrors.Errorf("failed to compile shader source for unknown reason")
	}
	return xerrors.Errorf("failed to compile shader source: %s", log)
}
