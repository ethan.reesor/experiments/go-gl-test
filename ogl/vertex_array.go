package ogl

import (
	"github.com/go-gl/gl/v4.6-core/gl"
)

func (vao VertexArray) Bind()                     { gl.BindVertexArray(vao.ID()) }
func (vao VertexArray) EnableAttrib(index uint32) { gl.EnableVertexArrayAttrib(vao.ID(), index) }
func (vao VertexArray) Unbind()                   { gl.BindVertexArray(0) }

func (vao VertexArray) LoadAttrib(index uint32, size int32, data []float32, usage Usage) {
	b := NewBuffer()

	vao.Bind()
	defer vao.Unbind()

	ArrayBuffer.Bind(b)
	defer ArrayBuffer.Unbind()

	vao.EnableAttrib(index)
	ArrayBuffer.SetData(data, usage)
	gl.VertexAttribPointer(index, size, gl.FLOAT, false, 0, nil)
}
