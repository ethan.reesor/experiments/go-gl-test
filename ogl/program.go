package ogl

import (
	"github.com/go-gl/gl/v4.6-core/gl"
	"golang.org/x/xerrors"
)

func LinkProgram(shaders ...Shader) (Program, error) {
	p := NewProgram()
	for _, s := range shaders {
		p.AttachShader(s)
	}

	err := p.Link()
	if err != nil {
		p.Delete()
		return Program{0}, nil
	}

	for _, s := range shaders {
		p.DetachShader(s)
		s.Delete()
	}

	return p, nil
}

func (s Program) GetLog() string {
	n := s.Get(gl.INFO_LOG_LENGTH)
	if n == 0 {
		return ""
	}

	log := string(make([]byte, n+1))
	gl.GetProgramInfoLog(s.ID(), n, nil, gl.Str(log))
	return log
}

func (p Program) Use()                  { gl.UseProgram(p.ID()) }
func (p Program) AttachShader(s Shader) { gl.AttachShader(p.ID(), s.ID()) }
func (p Program) DetachShader(s Shader) { gl.DetachShader(p.ID(), s.ID()) }

func (p Program) Link() error {
	gl.LinkProgram(p.ID())

	if p.Get(gl.LINK_STATUS) == gl.TRUE {
		return nil
	}

	log := p.GetLog()
	if log == "" {
		return xerrors.Errorf("failed to link program for unknown reason")
	}
	return xerrors.Errorf("failed to link program: %s", log)
}

func (p Program) GetUniformLocation(name string) Uniform {
	id := gl.GetUniformLocation(p.ID(), gl.Str(name+"\x00"))
	return Uniform(id)
}
