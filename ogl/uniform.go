package ogl

import (
	"github.com/go-gl/gl/v4.6-core/gl"
	glm "github.com/go-gl/mathgl/mgl32"
)

func (u Uniform) ID() int32 { return int32(u) }

func (u Uniform) Set1f(v0 float32)             { gl.Uniform1f(u.ID(), v0) }
func (u Uniform) Set2f(v0, v1 float32)         { gl.Uniform2f(u.ID(), v0, v1) }
func (u Uniform) Set3f(v0, v1, v2 float32)     { gl.Uniform3f(u.ID(), v0, v1, v2) }
func (u Uniform) Set4f(v0, v1, v2, v3 float32) { gl.Uniform4f(u.ID(), v0, v1, v2, v3) }
func (u Uniform) Set1d(v0 float64)             { gl.Uniform1d(u.ID(), v0) }
func (u Uniform) Set2d(v0, v1 float64)         { gl.Uniform2d(u.ID(), v0, v1) }
func (u Uniform) Set3d(v0, v1, v2 float64)     { gl.Uniform3d(u.ID(), v0, v1, v2) }
func (u Uniform) Set4d(v0, v1, v2, v3 float64) { gl.Uniform4d(u.ID(), v0, v1, v2, v3) }
func (u Uniform) Set1i(v0 int32)               { gl.Uniform1i(u.ID(), v0) }
func (u Uniform) Set2i(v0, v1 int32)           { gl.Uniform2i(u.ID(), v0, v1) }
func (u Uniform) Set3i(v0, v1, v2 int32)       { gl.Uniform3i(u.ID(), v0, v1, v2) }
func (u Uniform) Set4i(v0, v1, v2, v3 int32)   { gl.Uniform4i(u.ID(), v0, v1, v2, v3) }
func (u Uniform) Set1ui(v0 uint32)             { gl.Uniform1ui(u.ID(), v0) }
func (u Uniform) Set2ui(v0, v1 uint32)         { gl.Uniform2ui(u.ID(), v0, v1) }
func (u Uniform) Set3ui(v0, v1, v2 uint32)     { gl.Uniform3ui(u.ID(), v0, v1, v2) }
func (u Uniform) Set4ui(v0, v1, v2, v3 uint32) { gl.Uniform4ui(u.ID(), v0, v1, v2, v3) }

func (u Uniform) Set1fv(v ...float32) { gl.Uniform1fv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set2fv(v ...float32) { gl.Uniform2fv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set3fv(v ...float32) { gl.Uniform3fv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set4fv(v ...float32) { gl.Uniform4fv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set1dv(v ...float64) { gl.Uniform1dv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set2dv(v ...float64) { gl.Uniform2dv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set3dv(v ...float64) { gl.Uniform3dv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set4dv(v ...float64) { gl.Uniform4dv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set1iv(v ...int32)   { gl.Uniform1iv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set2iv(v ...int32)   { gl.Uniform2iv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set3iv(v ...int32)   { gl.Uniform3iv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set4iv(v ...int32)   { gl.Uniform4iv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set1uiv(v ...uint32) { gl.Uniform1uiv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set2uiv(v ...uint32) { gl.Uniform2uiv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set3uiv(v ...uint32) { gl.Uniform3uiv(u.ID(), int32(len(v)), &v[0]) }
func (u Uniform) Set4uiv(v ...uint32) { gl.Uniform4uiv(u.ID(), int32(len(v)), &v[0]) }

func (u Uniform) SetMatrix2fv(transpose bool, v ...glm.Mat2) {
	gl.UniformMatrix2fv(u.ID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) SetMatrix3fv(transpose bool, v ...glm.Mat3) {
	gl.UniformMatrix3fv(u.ID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) SetMatrix4fv(transpose bool, v ...glm.Mat4) {
	gl.UniformMatrix4fv(u.ID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) SetMatrix2x3fv(transpose bool, v ...glm.Mat2x3) {
	gl.UniformMatrix2x3fv(u.ID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) SetMatrix3x2fv(transpose bool, v ...glm.Mat3x2) {
	gl.UniformMatrix3x2fv(u.ID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) SetMatrix2x4fv(transpose bool, v ...glm.Mat2x4) {
	gl.UniformMatrix2x4fv(u.ID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) SetMatrix4x2fv(transpose bool, v ...glm.Mat4x2) {
	gl.UniformMatrix4x2fv(u.ID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) SetMatrix3x4fv(transpose bool, v ...glm.Mat3x4) {
	gl.UniformMatrix3x4fv(u.ID(), int32(len(v)), transpose, &v[0][0])
}

func (u Uniform) SetMatrix4x3fv(transpose bool, v ...glm.Mat4x3) {
	gl.UniformMatrix4x3fv(u.ID(), int32(len(v)), transpose, &v[0][0])
}
