package ogl

import (
	"github.com/go-gl/gl/v4.6-core/gl"
)

type Name uint32
type Uniform int32

type Buffer struct{ Name }
type BufferTarget struct{ Name }
type Framebuffer struct{ Name }
type Primitive struct{ Name }
type Program struct{ Name }
type ProgramPipeline struct{ Name }
type Query struct{ Name }
type QueryTarget struct{ Name }
type Renderbuffer struct{ Name }
type Sampler struct{ Name }
type Shader struct{ Name }
type ShaderType struct{ Name }
type Texture struct{ Name }
type TextureTarget struct{ Name }
type TransformFeedback struct{ Name }
type VertexArray struct{ Name }
type Usage struct{ Name }

func NewProgram() Program          { return Program{Name(gl.CreateProgram())} }
func (typ ShaderType) New() Shader { return Shader{Name(gl.CreateShader(typ.ID()))} }

func NewBuffers(n int32) []Buffer {
	id := make([]uint32, n)
	gl.CreateBuffers(n, &id[0])

	r := make([]Buffer, n)
	for i, id := range id {
		r[i] = Buffer{Name(id)}
	}
	return r
}

func NewBuffer() Buffer {
	var id uint32
	gl.CreateBuffers(1, &id)
	return Buffer{Name(id)}
}

func NewFramebuffer() Framebuffer {
	var id uint32
	gl.CreateFramebuffers(1, &id)
	return Framebuffer{Name(id)}
}

func NewProgramPipeline() ProgramPipeline {
	var id uint32
	gl.CreateProgramPipelines(1, &id)
	return ProgramPipeline{Name(id)}
}

func NewRenderbuffer() Renderbuffer {
	var id uint32
	gl.CreateRenderbuffers(1, &id)
	return Renderbuffer{Name(id)}
}

func NewSampler() Sampler {
	var id uint32
	gl.CreateSamplers(1, &id)
	return Sampler{Name(id)}
}

func NewVertexArray() VertexArray {
	var id uint32
	gl.CreateVertexArrays(1, &id)
	return VertexArray{Name(id)}
}

func NewTransformFeedback() TransformFeedback {
	var id uint32
	gl.CreateTransformFeedbacks(1, &id)
	return TransformFeedback{Name(id)}
}

func (target QueryTarget) New() Query {
	var id uint32
	gl.CreateQueries(target.ID(), 1, &id)
	return Query{Name(id)}
}

func (target TextureTarget) New() Texture {
	var id uint32
	gl.CreateTextures(target.ID(), 1, &id)
	return Texture{Name(id)}
}

func (p Program) Delete() { gl.DeleteProgram(p.ID()) }
func (s Shader) Delete()  { gl.DeleteShader(s.ID()) }

func (n Buffer) Delete()            { id := n.ID(); gl.DeleteBuffers(1, &id) }
func (n Framebuffer) Delete()       { id := n.ID(); gl.DeleteFramebuffers(1, &id) }
func (n ProgramPipeline) Delete()   { id := n.ID(); gl.DeleteProgramPipelines(1, &id) }
func (n Query) Delete()             { id := n.ID(); gl.DeleteQueries(1, &id) }
func (n Renderbuffer) Delete()      { id := n.ID(); gl.DeleteRenderbuffers(1, &id) }
func (n Sampler) Delete()           { id := n.ID(); gl.DeleteSamplers(1, &id) }
func (n Texture) Delete()           { id := n.ID(); gl.DeleteTextures(1, &id) }
func (n TransformFeedback) Delete() { id := n.ID(); gl.DeleteTransformFeedbacks(1, &id) }
func (n VertexArray) Delete()       { id := n.ID(); gl.DeleteVertexArrays(1, &id) }
