package graphics

type Alert int

const (
	InvalidAlert Alert = iota
	EscapeKey
)

type action0 int

const (
	Close action0 = iota
)
