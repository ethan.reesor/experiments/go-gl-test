package graphics

const vertexShaderSource = `
	#version 460
	
	in vec3 vp;
	in vec3 color;
	in vec2 uv;

	out vec3 _color;
	out vec2 _uv;

	uniform mat4 model;
	uniform mat4 view;
	uniform mat4 projection;

    void main() {
		gl_Position = projection * view * model * vec4(vp, 1.0);
		_color = color;
		_uv = uv;
    }
`

const fragmentShaderSource = `
	#version 460
	
	in vec3 _color;
	in vec2 _uv;

	out vec3 color;

	uniform sampler2D tex;
	
    void main() {
        color = 0.5 * _color + 0.5 * texture(tex, _uv).rgb;
    }
`

const skyboxVertexShader = `
	#version 460
	in vec3 vp;
	in vec3 color;
	out vec3 dir;
	out vec3 _color;

	uniform mat4 model;
	uniform mat4 view;
	uniform mat4 projection;
	
	void main()
	{
		dir = vp;
		gl_Position = projection * view * model * vec4(vp, 1.0);
	}  
`

const skyboxFragmentShader = `
	#version 460
	in vec3 dir;
	in vec3 _color;
	out vec3 color;

	uniform samplerCube skybox;
	
	void main()
	{
		color = texture(skybox, dir).rgb;
		// color = _color;
	}  
`
