package graphics

import (
	_ "image/jpeg"
	_ "image/png"
	"log"
	"math"
	"unsafe"

	"github.com/go-gl/gl/v4.6-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	glm "github.com/go-gl/mathgl/mgl32"
	"gitlab.com/ethan.reesor/go-gl-tests/ogl"
	_ "golang.org/x/image/bmp"
	"golang.org/x/xerrors"
)

const (
	Float32Size = int(unsafe.Sizeof(float32(0)))

	moveSpeed   = 10
	zoomSpeed   = 0.1
	rotateSpeed = 0.1
)

type render struct {
	mainProgram        ogl.Program
	mainVertexShader   ogl.Shader
	mainFragmentShader ogl.Shader
	mainObject         ogl.VertexArray
	mainModel          ogl.Uniform
	mainView           ogl.Uniform
	mainProjection     ogl.Uniform
	mainTexture        ogl.Texture

	skyboxProgram        ogl.Program
	skyboxVertexShader   ogl.Shader
	skyboxFragmentShader ogl.Shader
	skyboxObject         ogl.VertexArray
	skyboxTexture        ogl.Texture
	skyboxModel          ogl.Uniform
	skyboxView           ogl.Uniform
	skyboxProjection     ogl.Uniform

	time       float64
	zoom       float64
	hrad, vrad float64
	position   glm.Vec3
}

func maybeLog(name, msg string) {
	if msg == "" {
		return
	}

	log.Printf("%s: %s", name, msg)
}

func (r *render) initMain() (err error) {
	r.mainVertexShader, err = ogl.VertexShader.Compile(vertexShaderSource)
	if err != nil {
		return err
	}

	r.mainFragmentShader, err = ogl.FragmentShader.Compile(fragmentShaderSource)
	if err != nil {
		return err
	}

	r.mainProgram, err = ogl.LinkProgram(r.mainVertexShader, r.mainFragmentShader)
	if err != nil {
		return err
	}

	maybeLog("main vertex shader", r.mainVertexShader.GetLog())
	maybeLog("main fragment shader", r.mainFragmentShader.GetLog())
	maybeLog("main program", r.mainProgram.GetLog())

	r.mainModel = r.mainProgram.GetUniformLocation("model")
	r.mainView = r.mainProgram.GetUniformLocation("view")
	r.mainProjection = r.mainProgram.GetUniformLocation("projection")

	r.mainObject = ogl.NewVertexArray()
	r.mainObject.LoadAttrib(0, 3, cubeVertices, ogl.StaticDraw) // vertices
	r.mainObject.LoadAttrib(1, 3, cubeVertices, ogl.StaticDraw) // color: rgb
	// r.mainObject.LoadAttrib(1, 3, cubeRandomColors, ogl.StaticDraw) // color: random
	r.mainObject.LoadAttrib(2, 2, cubeUV, ogl.StaticDraw) // uv

	r.mainTexture, err = ogl.Texture2D.NewFromImageFile("grid512.bmp")
	if err != nil {
		return xerrors.Errorf("failed to load texture: %w", err)
	}

	ogl.Texture2D.Bind(r.mainTexture)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	ogl.Texture2D.Unbind()

	return nil
}

func (r *render) initSkybox() (err error) {
	r.skyboxObject = ogl.NewVertexArray()
	r.skyboxObject.LoadAttrib(0, 3, skyboxVertices, ogl.StaticDraw) // vertices
	r.skyboxObject.LoadAttrib(1, 3, cubeVertices, ogl.StaticDraw)   // color: rgb

	r.skyboxTexture, err = ogl.NewCubemapFromImageFiles(
		"skybox/right.jpg",
		"skybox/left.jpg",
		"skybox/top.jpg",
		"skybox/bottom.jpg",
		"skybox/front.jpg",
		"skybox/back.jpg",
	)
	if err != nil {
		return xerrors.Errorf("failed to load texture: %w", err)
	}

	r.skyboxVertexShader, err = ogl.VertexShader.Compile(skyboxVertexShader)
	if err != nil {
		return err
	}

	r.skyboxFragmentShader, err = ogl.FragmentShader.Compile(skyboxFragmentShader)
	if err != nil {
		return err
	}

	r.skyboxProgram, err = ogl.LinkProgram(r.skyboxVertexShader, r.skyboxFragmentShader)
	if err != nil {
		return err
	}

	r.skyboxModel = r.skyboxProgram.GetUniformLocation("model")
	r.skyboxView = r.skyboxProgram.GetUniformLocation("view")
	r.skyboxProjection = r.skyboxProgram.GetUniformLocation("projection")

	maybeLog("skybox vertex shader", r.skyboxVertexShader.GetLog())
	maybeLog("skybox fragment shader", r.skyboxFragmentShader.GetLog())
	maybeLog("skybox program", r.skyboxProgram.GetLog())

	ogl.TextureCubeMap.Bind(r.skyboxTexture)
	gl.TexParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	ogl.TextureCubeMap.Unbind()

	return nil
}

func (r *render) init(w *glfw.Window) error {
	var err error

	r.time = glfw.GetTime()
	r.zoom = 1
	r.hrad = math.Pi
	r.position = glm.Vec3{0, 0, 5}

	err = r.initMain()
	if err != nil {
		return err
	}

	err = r.initSkybox()
	if err != nil {
		return err
	}

	// set the background (clear) color
	gl.ClearColor(0, 0.5, 1.0, 1.0)

	gl.Enable(gl.CULL_FACE)
	gl.Enable(gl.DEPTH_TEST)
	gl.DepthFunc(gl.LESS)

	width, height := w.GetFramebufferSize()
	w.SetCursorPos(float64(width)/2, float64(height)/2)

	w.SetScrollCallback(func(_ *glfw.Window, xoff float64, yoff float64) {
		r.zoom += zoomSpeed * yoff
		if r.zoom < 0.1 {
			r.zoom = 0.1
		}
		if r.zoom > 2 {
			r.zoom = 2
		}
	})

	return nil
}

func (r *render) drawSkybox(model, view, projection glm.Mat4) {
	gl.DepthMask(false)
	defer gl.DepthMask(true)

	r.skyboxProgram.Use()
	r.skyboxModel.SetMatrix4fv(false, model)
	r.skyboxView.SetMatrix4fv(false, view)
	r.skyboxProjection.SetMatrix4fv(false, projection)

	r.skyboxObject.Bind()
	defer r.skyboxObject.Unbind()

	ogl.TextureCubeMap.Bind(r.skyboxTexture)
	defer ogl.TextureCubeMap.Unbind()

	ogl.Triangles.Draw(0, 36)
}

func (r *render) drawMain(model, view, projection glm.Mat4) {
	r.mainProgram.Use()
	r.mainModel.SetMatrix4fv(false, model)
	r.mainView.SetMatrix4fv(false, view)
	r.mainProjection.SetMatrix4fv(false, projection)

	r.mainObject.Bind()
	defer r.mainObject.Unbind()

	ogl.Texture2D.Bind(r.mainTexture)
	defer ogl.Texture2D.Unbind()

	ogl.Triangles.Draw(0, int32(len(cubeVertices)))
}

func (r *render) frame(w *glfw.Window) {
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	time := glfw.GetTime()
	elapsed := time - r.time
	r.time = time

	width, height := w.GetFramebufferSize()
	xc, yc := w.GetCursorPos()
	w.SetCursorPos(float64(width)/2, float64(height)/2)

	r.hrad += rotateSpeed * elapsed * (float64(width/2) - xc)
	r.vrad += rotateSpeed * elapsed * (float64(height/2) - yc)

	direction := glm.Vec3{
		float32(math.Cos(r.vrad) * math.Sin(r.hrad)),
		float32(math.Sin(r.vrad)),
		float32(math.Cos(r.vrad) * math.Cos(r.hrad)),
	}
	right := glm.Vec3{
		float32(-math.Cos(r.hrad)),
		float32(0),
		float32(math.Sin(r.hrad)),
	}
	up := right.Cross(direction)

	if w.GetKey(glfw.KeyW) == glfw.Press {
		r.position = r.position.Add(direction.Mul(float32(elapsed) * moveSpeed))
	}
	if w.GetKey(glfw.KeyS) == glfw.Press {
		r.position = r.position.Sub(direction.Mul(float32(elapsed) * moveSpeed))
	}
	if w.GetKey(glfw.KeyA) == glfw.Press {
		r.position = r.position.Sub(right.Mul(float32(elapsed) * moveSpeed))
	}
	if w.GetKey(glfw.KeyD) == glfw.Press {
		r.position = r.position.Add(right.Mul(float32(elapsed) * moveSpeed))
	}
	if w.GetKey(glfw.KeySpace) == glfw.Press {
		r.position = r.position.Add(up.Mul(float32(elapsed) * moveSpeed))
	}
	if w.GetKey(glfw.KeyLeftShift) == glfw.Press {
		r.position = r.position.Sub(up.Mul(float32(elapsed) * moveSpeed))
	}

	proj := glm.Perspective(glm.DegToRad(45), float32(width)/float32(height), 0.1, 300)
	view := glm.LookAtV(
		r.position,
		r.position.Add(direction),
		up,
	)
	// view := glm.LookAtV(
	// 	glm.Vec3{4, 3, 3},
	// 	glm.Vec3{0, 0, 0},
	// 	glm.Vec3{0, 1, 0},
	// )
	model := glm.Scale3D(float32(r.zoom), float32(r.zoom), float32(r.zoom))

	r.drawSkybox(model, view, proj)
	r.drawMain(model, view, proj)
}
