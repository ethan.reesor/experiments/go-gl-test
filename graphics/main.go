package graphics

import (
	"github.com/go-gl/gl/v4.6-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"gitlab.com/ethan.reesor/go-gl-tests/messaging"
	"golang.org/x/xerrors"
)

func Main(bus *messaging.Bus) error {
	act := messaging.Must(bus.Consumer(Close)).(<-chan action0)
	alert := messaging.Must(bus.Producer(InvalidAlert)).(chan<- Alert)

	defer func() {
		close(alert)
	}()

	err := glfw.Init()
	if err != nil {
		return xerrors.Errorf("failed to initialize GLFW: %w", err)
	}
	defer glfw.Terminate()

	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 6)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)
	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.Decorated, glfw.True)

	w, err := glfw.CreateWindow(1600, 1000, "Go GL Test", nil, nil)
	if err != nil {
		return xerrors.Errorf("failed to create window: %w", err)
	}

	w.MakeContextCurrent()

	err = gl.Init()
	if err != nil {
		return xerrors.Errorf("failed to initialize OpenGL: %w", err)
	}

	// this lets us capture ESC?
	w.SetInputMode(glfw.StickyKeysMode, glfw.True)
	w.SetInputMode(glfw.CursorMode, glfw.CursorHidden)

	r := new(render)
	err = r.init(w)
	if err != nil {
		return xerrors.Errorf("failed to initialize render process: %w", err)
	}

main:
	for !w.ShouldClose() {
		r.frame(w)
		w.SwapBuffers()
		glfw.PollEvents()

		if w.GetKey(glfw.KeyEscape) == glfw.Press {
			alert <- EscapeKey
		}

		for {
			select {
			case x := <-act:
				switch x {
				case Close:
					w.SetShouldClose(true)
				}

			default:
				continue main
			}
		}
	}

	return nil
}
